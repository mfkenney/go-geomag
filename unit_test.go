package geomag

import (
	"math"
	"testing"
	"time"
)

var T1 = time.Date(2015, time.January, 1, 0, 0, 0, 0, time.UTC)
var T2 = time.Date(2017, time.July, 2, 0, 0, 0, 0, time.UTC)
var TABLE = []struct {
	t   time.Time
	p   Point
	dec float64
}{
	{t: T1, p: Point{Alt: 0, Lat: 80, Lon: 0}, dec: -3.85},
	{t: T1, p: Point{Alt: 0, Lat: 0, Lon: 120}, dec: 0.57},
	{t: T1, p: Point{Alt: 0, Lat: -80, Lon: 240}, dec: 69.81},
	{t: T1, p: Point{Alt: 100000.0, Lat: 80, Lon: 0}, dec: -4.27},
	{t: T1, p: Point{Alt: 100000.0, Lat: 0, Lon: 120}, dec: 0.56},
	{t: T1, p: Point{Alt: 100000.0, Lat: -80, Lon: 240}, dec: 69.22},
	{t: T2, p: Point{Alt: 0, Lat: 80, Lon: 0}, dec: -2.75},
	{t: T2, p: Point{Alt: 0, Lat: 0, Lon: 120}, dec: 0.32},
	{t: T2, p: Point{Alt: 0, Lat: -80, Lon: 240}, dec: 69.58},
	{t: T2, p: Point{Alt: 100000.0, Lat: 80, Lon: 0}, dec: -3.17},
	{t: T2, p: Point{Alt: 100000.0, Lat: 0, Lon: 120}, dec: 0.32},
	{t: T2, p: Point{Alt: 100000.0, Lat: -80, Lon: 240}, dec: 69.00},
	{t: T2, p: Point{Alt: 0, Lat: 47, Lon: -122}, dec: 15.58},
}

func relerr(x, y float64) float64 {
	return math.Abs(y-x) / y
}

func TestDeclination(t *testing.T) {
	s := NewState(nil)
	for _, e := range TABLE {
		dec := s.Declination(e.p, e.t)
		if relerr(dec, e.dec) > 0.01 {
			t.Errorf("Declination(%v, %v): got %.3f expected %.3f\n",
				e.p, e.t, dec, e.dec)
		}
	}
}
