// Package geomag uses the NGDC World Magnetic Model to calculate the
// magnetic declination for a given geodetic position. The code was
// adapted from the Python geomag package:
//
//    https://github.com/cmweiss/geomag
//
package geomag

import (
	"bufio"
	"bytes"
	"io"
	"math"
	"strconv"
	"strings"
	"time"
)

const (
	MAXORD    int = 12
	MAXDEGREE int = 12
)

const (
	A  float64 = 6378.137
	B  float64 = 6356.7523142
	RE float64 = 6371.2
)

// Point represents a geodetic location
type Point struct {
	// Latitude in degrees
	Lat float64
	// Longitude in degrees
	Lon float64
	// Altitude in meters above mean sea level
	Alt float64
}

type WmmEntry struct {
	N    int
	M    int
	Gnm  float64
	Hnm  float64
	Dgnm float64
	Dhnm float64
}

type Wmm struct {
	Epoch   float64
	Model   string
	Date    string
	Entries []WmmEntry
}

type State struct {
	epoch      float64
	tc         [14][14]float64
	sp         [15]float64
	cp         [15]float64
	pp         [14]float64
	p          [14][15]float64
	dp         [14][14]float64
	c          [14][15]float64
	cd         [14][15]float64
	snorm      [13][14]float64
	k          [13][14]float64
	fn, fm     [13]float64
	a2, b2, c2 float64
	a4, b4, c4 float64
}

func parseEntry(f []string, e *WmmEntry) error {
	var (
		ix  int64
		err error
	)

	if ix, err = strconv.ParseInt(f[0], 10, 32); err != nil {
		return err
	}
	e.N = int(ix)

	if ix, err = strconv.ParseInt(f[1], 10, 32); err != nil {
		return err
	}
	e.M = int(ix)

	if e.Gnm, err = strconv.ParseFloat(f[2], 32); err != nil {
		return err
	}

	if e.Hnm, err = strconv.ParseFloat(f[3], 32); err != nil {
		return err
	}

	if e.Dgnm, err = strconv.ParseFloat(f[4], 32); err != nil {
		return err
	}

	if e.Dhnm, err = strconv.ParseFloat(f[5], 32); err != nil {
		return err
	}

	return nil
}

// LoadWmm reads the WMM coeffiencients from a file
func LoadWmm(rdr io.Reader) (*Wmm, error) {
	var err error
	w := Wmm{
		Entries: make([]WmmEntry, 0),
	}
	scanner := bufio.NewScanner(rdr)
	line := int(0)
	for scanner.Scan() {
		raw := strings.Split(scanner.Text(), " ")
		if len(raw) <= 1 {
			break
		}

		// Skip the empty fields
		fields := make([]string, 0)
		for _, f := range raw {
			if f != "" {
				fields = append(fields, f)
			}
		}

		if line == 0 {
			w.Epoch, err = strconv.ParseFloat(fields[0], 64)
			if err != nil {
				return nil, err
			}
			w.Model = fields[1]
			w.Date = fields[2]
		} else {
			e := WmmEntry{}
			err = parseEntry(fields, &e)
			if err != nil {
				return nil, err
			}
			w.Entries = append(w.Entries, e)
		}
		line++
	}

	if err = scanner.Err(); err != nil {
		return nil, err
	}

	return &w, nil
}

// NewState creates a new State struct from the WMM coeffiecents. If w is
// nil, the compiled-in coefficients are used.
func NewState(w *Wmm) *State {
	if w == nil {
		b := bytes.NewBuffer(WMM_COF)
		w, _ = LoadWmm(b)
	}
	s := State{}
	s.epoch = w.Epoch
	s.cp[0] = 1
	s.pp[0] = 1
	s.p[0][0] = 1
	s.a2 = A * A
	s.b2 = B * B
	s.c2 = s.a2 - s.b2
	s.a4 = s.a2 * s.a2
	s.b4 = s.b2 * s.b2
	s.c4 = s.a4 - s.b4

	for _, e := range w.Entries {
		if e.M <= e.N {
			s.c[e.M][e.N] = e.Gnm
			s.cd[e.M][e.N] = e.Dgnm
			if e.M != 0 {
				s.c[e.N][e.M-1] = e.Hnm
				s.cd[e.N][e.M-1] = e.Dhnm
			}
		}
	}

	s.snorm[0][0] = 1.0
	for n := 1; n <= MAXORD; n++ {
		s.snorm[0][n] = s.snorm[0][n-1] * (2.0*float64(n) - 1) / float64(n)
		j := float64(2)
		m := int(0)
		d2 := n - m + 1
		for d2 > 0 {
			s.k[m][n] = ((float64(n-1) * float64(n-1)) - float64(m*m)) / ((2.0*float64(n) - 1) * (2.0*float64(n) - 3.0))
			if m > 0 {
				flnmj := (float64(n-m+1.0) * j) / float64(n+m)
				s.snorm[m][n] = s.snorm[m-1][n] * math.Sqrt(flnmj)
				j = 1.0
				s.c[n][m-1] = s.snorm[m][n] * s.c[n][m-1]
				s.cd[n][m-1] = s.snorm[m][n] * s.cd[n][m-1]
			}
			s.c[m][n] = s.snorm[m][n] * s.c[m][n]
			s.cd[m][n] = s.snorm[m][n] * s.cd[m][n]
			d2--
			m++
		}
	}

	return &s
}

func radians(deg float64) float64 {
	return deg * math.Pi / 180.0
}

func degrees(rad float64) float64 {
	return rad * 180.0 / math.Pi
}

// Calculate the declination for the magnetic heading at the given
// geodetic position and point in time.
func (s *State) Declination(p Point, t time.Time) float64 {
	// Convert time to years since the WMM epoch
	t0 := time.Date(t.Year(), time.January, 1, 0, 0, 0, 0, t.Location())
	days := float64(t.Sub(t0)) / 1e9 / 86400.0
	years := float64(t.Year()) + days/365.0
	dt := years - s.epoch

	// Convert altitude to kilometers
	alt := p.Alt / 1000.0

	otime := -1000.0
	oalt := -1000.0
	olat := -1000.0
	olon := -1000.0

	fn := []float64{0.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0}
	fm := []float64{0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0}

	glat := p.Lat
	glon := p.Lon
	rlat := radians(glat)
	rlon := radians(glon)
	srlon := math.Sin(rlon)
	srlat := math.Sin(rlat)
	crlon := math.Cos(rlon)
	crlat := math.Cos(rlat)
	srlat2 := srlat * srlat
	crlat2 := crlat * crlat
	s.sp[1] = srlon
	s.cp[1] = crlon

	var ca, sa, ct, st, r float64
	if alt != oalt || glat != olat {
		q := math.Sqrt(s.a2 - s.c2*srlat2)
		q1 := alt * q
		q2 := ((q1 + s.a2) / (q1 + s.b2)) * ((q1 + s.a2) / (q1 + s.b2))
		ct = srlat / math.Sqrt(q2*crlat2+srlat2)
		st = math.Sqrt(1.0 - (ct * ct))
		r2 := (alt * alt) + 2.0*q1 + (s.a4-s.c4*srlat2)/(q*q)
		r = math.Sqrt(r2)
		d := math.Sqrt(s.a2*crlat2 + s.b2*srlat2)
		ca = (alt + d) / r
		sa = s.c2 * crlat * srlat / (r * d)
	}

	if glon != olon {
		for m := 2; m <= MAXORD; m++ {
			s.sp[m] = s.sp[1]*s.cp[m-1] + s.cp[1]*s.sp[m-1]
			s.cp[m] = s.cp[1]*s.cp[m-1] - s.sp[1]*s.sp[m-1]
		}
	}

	aor := RE / r
	ar := aor * aor
	var br, bt, bp, bpp float64
	for n := 1; n <= MAXORD; n++ {
		ar = ar * aor
		m := int(0)
		for d4 := n + m + 1; d4 > 0; d4-- {
			if alt != oalt || glat != olat {
				if n == m {
					s.p[m][n] = st * s.p[m-1][n-1]
					s.dp[m][n] = st*s.dp[m-1][n-1] + ct*s.p[m-1][n-1]
				} else if n == 1 && m == 0 {
					s.p[m][n] = ct * s.p[m][n-1]
					s.dp[m][n] = ct*s.dp[m][n-1] - st*s.p[m][n-1]
				} else if n > 1 && n != m {
					if m > (n - 2) {
						s.p[m][n-2] = 0
					}
					s.p[m][n] = ct*s.p[m][n-1] - s.k[m][n]*s.p[m][n-2]
					s.dp[m][n] = ct*s.dp[m][n-1] - st*s.p[m][n-1] - s.k[m][n]*s.dp[m][n-2]
				}
			}

			if years != otime {
				s.tc[m][n] = s.c[m][n] + dt*s.cd[m][n]
				if m != 0 {
					s.tc[n][m-1] = s.c[n][m-1] + dt*s.cd[n][m-1]
				}
			}

			par := ar * s.p[m][n]
			var temp1, temp2 float64
			if m == 0 {
				temp1 = s.tc[m][n] * s.cp[m]
				temp2 = s.tc[m][n] * s.sp[m]
			} else {
				temp1 = s.tc[m][n]*s.cp[m] + s.tc[n][m-1]*s.sp[m]
				temp2 = s.tc[m][n]*s.sp[m] - s.tc[n][m-1]*s.cp[m]
			}

			bt = bt - ar*temp1*s.dp[m][n]
			bp = bp + (fm[m] * temp2 * par)
			br = br + (fn[n] * temp1 * par)

			if st == 0.0 && m == 1 {
				if n == 1 {
					s.pp[n] = s.pp[n-1]
				} else {
					s.pp[n] = ct*s.pp[n-1] - s.k[m][n]*s.pp[n-2]
				}
				parp := ar * s.pp[n]
				bpp = bpp + (fm[m] * temp2 * parp)
			}
			m++
		}
	}

	if st == 0 {
		bp = bpp
	} else {
		bp = bp / st
	}

	var bx, by float64

	bx = -bt*ca - br*sa
	by = bp

	return degrees(math.Atan2(by, bx))
}
